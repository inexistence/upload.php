upload.php
==========

a php script for file uploads. allows uploading ANY file with a configurable
size limit.

how to use: post `file` via multipart/form-data to /upload.php and receive a
link to the file. it'll have a random name.

the apache config is extremely important: it turns off the php engine in the
upload folder to allow .php uploads without actually running them. additionally,
the server will force download of all files using the `Content-Disposition:
attachment` http header.

png/gif/jpg uploads are treated differently based on mime sniffing of the first
4 bytes of the file. normally, all files are forced to be downloaded and given
a random name, but these sniffed files will have an extension (like `.jpeg`) and
the server won't force a file download, to allow for viewing in a browser.
more of these cases can be added by fiddling with $magic in upload.php AND
matching the extension in the bottom of the apache config file.

an example html form has been included in index.html.