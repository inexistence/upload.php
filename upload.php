<?php

if ($_SERVER['REQUEST_METHOD'] != 'POST') {
	echo 'not a POST request';
	die;
}

if (!isset($_FILES) || empty($_FILES) || !array_key_exists('file', $_FILES)) {
	echo 'u didnt upload anything. make sure u POST to `file`';
	die;
}

$file = $_FILES['file'];

if (!array_key_exists('size', $file) || !isset($file['size'])) {
	echo 'somehow ur upload has no size';
	die;
}

if ($file['size'] > 8388608) { // 8 MiB
	echo 'too large';
	die;
}

function try_get_filetype($file) {
	// match these things to the content-disposition server config
	$magic = [
		'png' => [ 4, '89504E47' ],
		'gif' => [ 4, '47494638' ],
		'jpg' => [ 2, 'FFD8' ]
	];

	foreach ($magic as $ext => $data) {
		$binary = file_get_contents($file, false, null, 0, $data[0]);
		// cant read file? fuck it
		if ($binary === false) {
			return null;
		}

		if ($data[1] === strtoupper(bin2hex($binary))) {
			return $ext;
		}
	}

	return null;
}

// https://stackoverflow.com/a/4356295/4301778
function rand_string($length = 8) {
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return $randomString;
}

$name = rand_string(8);
if ($file['size'] > 128) {
	$ext = try_get_filetype($file['tmp_name']);

	if ($ext !== null) {
		$name .= '.' . $ext;
	}
}

$target = 'stuff/' . $name;

$success = move_uploaded_file($file['tmp_name'], $target);

if ($success === true) {
	header('Location: ' . $target);
	echo $target;
} else {
	echo 'failed for some reason';
}
